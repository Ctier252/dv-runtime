from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("da160311543285d34f85ffe2c6c35ba340036164b5935b714260f1c798d02120")
@calibration_required
def test_12_02_UndistortFrames(self):
    calib_file = test_params["calibration_file"]

    # Check condition to run
    check_run(self)

    # Check calibration file
    self.assertEqual(os.path.exists(calib_file), True, 'Check if calibration file exists:{}'.format(calib_file))

    module_parameters = [["fitMorePixels", "0.2"], ["calibrationFile", calib_file]]
    module = dv_module("dv_undistort", [["frames", "input[frames]"]], ["undistortedFrames"],
                       config_options=module_parameters)
    test_modules_with_io(self, [module])
