from test_framework._support import *
import requests
from datetime import datetime
import numpy as np
from test_framework._test_params import *


@dvxplorer_performance_input
@test_performance
def test_15_11_perf_ynoise(self):
    module = [dv_module("dv_ynoise", [["events", "input[events]"]], ["events"])]
    input_params = [["seekStart", "0"], ["seekEnd", 2000000], ["logLevel", "ERROR"]]
    test_modules_with_input(self, module, input_parameters=input_params)
