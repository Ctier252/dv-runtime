from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
# Exponential decay is calculated subtly differently on different platforms.
# Therefore we need to provide different expected results for each platform.
@output_hash([
    "8bb2075bd8c382371283d81a8685e41cc35f0a7d5e1b7dda8eaf818619b884f2",  # OS X Intel
    "6f31aeb0390452e734782e8dc604dfcfc0f854c6acb3aff5e8f4b9ffe16d0785",  # OS X ARM
    "2b60564e48737209dfcdab896805b97008d51232bd594bd0d335bde6a735d16b",  # Ubuntu
    "b3523c80ebfa606d4a07fdd0ee5e44eae88bdbff196b2ec2b9936cbe35072a92"  # Windows
])
def test_05_01_accumulatorPlayFile(self):
    module = dv_module("dv_accumulator", [["events", "input[events]"]], ["frames"])
    test_modules_with_io(self, [module])
