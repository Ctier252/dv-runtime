from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_file_size([0.48 * dvxplorer_file["size"], 0.52 * dvxplorer_file["size"]])
def test_13_decimation(self):
    module_parameters = [["percentage", "50"]]
    module = dv_module("dv_decimation", [["events", "input[events]"]], ["events"], config_options=module_parameters)
    test_modules_with_io(self, [module])
