from test_framework._support import *
from test_framework._test_params import *


@dvxplorer_input
@output_hash("e726d7624ea66ae25cf8527427c93f630dfa0f28b13b4311d8624ed2153e5db7")
def test_11_ExportCSV(self):
    output_file = create_output_file_name_csv()
    module_parameters = [["fileName", output_file]]
    input_parameters = [["seekEnd", 5000]]

    module = dv_module("output0", [["events", "input[events]"]],
                       config_options=module_parameters,
                       library="dv_export_csv")
    test_modules_with_input(self, [module], input_parameters)
