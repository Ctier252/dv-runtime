from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash([
    "90d3d7b29e1cc021807ee0d316054795e407a0434c54d026c29725d025e4b497",
    "a2c52bba201f0be95a058652ebc8d3edb0ab16d883c85e35c7f3ae08fdd22146"
])
def test_09_03_FrameContrastClahe(self):
    module_parameters = [["contrastAlgorithm", "clahe"]]
    module = dv_module("dv_frame_contrast", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
