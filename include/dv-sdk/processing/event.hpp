#ifndef DV_PROCESSING_EVENT_HPP
#define DV_PROCESSING_EVENT_HPP

#include "core.hpp"

namespace dv {

/**
 * Function that creates perfect hash for 2d coordinates.
 * @param x x coordinate
 * @param y y coordinate
 * @return a 64 bit hash that uniquely identifies the coordinates
 */
inline uint32_t coordinateHash(const int16_t x, const int16_t y) {
	return (static_cast<uint32_t>(x) << 16u) | static_cast<uint32_t>(y);
}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
/**
 * Extracts only the events that are within the defined region of interest.
 * This function copies the events from the in EventStore into the given
 * out EventStore, if they intersect with the given region of interest rectangle.
 * @param in The EventStore to operate on. Won't be modified.
 * @param out The EventStore to put the ROI events into. Will get modified.
 * @param roi The rectangle with the region of interest.
 */
inline void roiFilter(const EventStore &in, EventStore &out, const cv::Rect &roi) {
	// in-place filtering is not supported
	assert(&in != &out);

	for (const Event &event : in) {
		if (roi.contains(cv::Point(event.x(), event.y()))) {
			out.add(event);
		}
	}
}
#endif

/**
 * Projects the event coordinates onto a smaller range. The x- and y-coordinates the divided
 * by xFactor and yFactor respectively and floored to the next integer. This forms the new
 * coordinates of the event. Due to the nature of this, it can happen that multiple events
 * end up happening simultaneously at the same location. This is still a valid event stream,
 * as time keeps monotonically increasing, but is something that is unlikely to be generated
 * by an event camera.
 * @param in The EventStore to operate on. Won't get modified
 * @param out The outgoing EventStore to store the projected events on
 * @param xDivision Division factor for the x-coordinate for the events
 * @param yDivision Division factor for the y-coordinate of the events
 */
inline void scale(const EventStore &in, EventStore &out, double xDivision, double yDivision) {
	// in-place filtering is not supported
	assert(&in != &out);

	for (const Event &event : in) {
		out.add(Event(event.timestamp(), static_cast<int16_t>(event.x() / xDivision),
			static_cast<int16_t>(event.y() / yDivision), event.polarity()));
	}
}

/**
 * Filters events by polarity. Only events that exhibit the same polarity as given in
 * polarity are kept.
 * @param in Incoming EventStore to operate on. Won't get modified.
 * @param out The outgoing EventStore to store the kept events on
 * @param polarity The polarity of the events that should be kept
 */
inline void polarityFilter(const EventStore &in, EventStore &out, bool polarity) {
	// in-place filtering is not supported
	assert(&in != &out);

	for (const Event &event : in) {
		if (event.polarity() == polarity) {
			out.add(event);
		}
	}
}

#if defined(DV_API_OPENCV_SUPPORT) && DV_API_OPENCV_SUPPORT == 1
/**
 * Computes and returns a rectangle with dimensions such that all the events
 * in the given `EventStore` fall into the bounding box.
 * @param packet The EventStore to work on
 * @return The smallest possible rectangle that contains all the events in packet.
 */
inline cv::Rect boundingRect(const EventStore &packet) {
	if (packet.isEmpty()) {
		return cv::Rect(0, 0, 0, 0);
	}

	int16_t minX = std::numeric_limits<int16_t>::max();
	int16_t maxX = 0;
	int16_t minY = std::numeric_limits<int16_t>::max();
	int16_t maxY = 0;

	for (const Event &event : packet) {
		minX = std::min(event.x(), minX);
		maxX = std::max(event.x(), maxX);
		minY = std::min(event.y(), minY);
		maxY = std::max(event.y(), maxY);
	}

	return cv::Rect(minX, minY, maxX - minX, maxY - minY);
}
#endif

} // namespace dv

#endif // DV_PROCESSING_EVENT_HPP
