#!/usr/bin/env sh

# $PLATFORM must be defined in environment!
# exit when any command fails
set -e

# Destination directory
mkdir -p dv-runtime-packaged-$PLATFORM/dv_modules

# dv-runtime binary
cp src/dv-runtime dv-runtime-packaged-$PLATFORM/
macpack -v -d dv_modules/libs dv-runtime-packaged-$PLATFORM/dv-runtime

# dv-control binary
cp utils/dv-control/dv-control dv-runtime-packaged-$PLATFORM/
macpack -v -d dv_modules/libs dv-runtime-packaged-$PLATFORM/dv-control

# dv-filestat binary
cp utils/dv-filestat/dv-filestat dv-runtime-packaged-$PLATFORM/
macpack -v -d dv_modules/libs dv-runtime-packaged-$PLATFORM/dv-filestat

# All available modules
for mod in $(find modules -type f -iname '*.dylib'); do
	echo "${mod}"
	cp "${mod}" dv-runtime-packaged-$PLATFORM/dv_modules/
	macpack -v -d libs dv-runtime-packaged-$PLATFORM/dv_modules/"$(basename ${mod})"
done
