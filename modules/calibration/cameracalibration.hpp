#ifndef CAMERACALIBRATION_HPP
#define CAMERACALIBRATION_HPP

#include "calibration.hpp"

class CameraCalibration : public Calibration {
public:
	CameraCalibration(dv::RuntimeConfig *config_, dv::RuntimeOutputs *outputs_, dv::RuntimeInput<dv::Frame> &input);
	bool isCalibrated() override;
	bool getInput() override;
	bool searchPattern() override;
	void checkImages() override;
	bool calibrate() override;
	void saveCalibrationData() override;

private:
	void writeToFile(cv::FileStorage &fs) override;
	std::string getDefaultFileName() override;
};

#endif // CAMERACALIBRATION_HPP
