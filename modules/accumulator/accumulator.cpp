#include "dv-sdk/module.hpp"
#include "dv-sdk/processing.hpp"

#include <opencv2/imgproc.hpp>

class Accumulator : public dv::ModuleBase {
private:
	dv::EventStreamSlicer slicer;
	dv::Accumulator frameAccumulator;
	int sliceJob;
	int demosaic             = 0;
	int64_t accumulationTime = -1;
	int64_t currentFrameTime = -1;
	std::string sliceMethod;

public:
	static const char *initDescription() {
		return "Accumulates events into a frame. Provides various configurations to tune the integration process";
	}

	static void initInputs(dv::InputDefinitionList &in) {
		in.addEventInput("events");
	}

	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addFrameOutput("frames");
	}

	static void initConfigOptions(dv::RuntimeConfig &configuration) {
		configuration.add(
			"sliceMethod", dv::ConfigOption::listOption(
							   "Method for slicing events, elapsed time between frames or number of events per frame",
							   0, {"TIME", "NUMBER"}));
		configuration.add("rectifyPolarity", dv::ConfigOption::boolOption("All events have positive contribution"));
		configuration.add("eventContribution",
			dv::ConfigOption::floatOption("The contribution of a single event", 0.04f, 0.0f, 1.0f));
		configuration.add("maxPotential", dv::ConfigOption::floatOption("Value at which to clip the integration", .3f));
		configuration.add(
			"neutralPotential", dv::ConfigOption::floatOption("Value to which the decay tends over time", 0.0));
		configuration.add("minPotential", dv::ConfigOption::floatOption("Value at which to clip the integration", 0.0));
		configuration.add("decayFunction", dv::ConfigOption::listOption("The decay function to be used", 2,
											   {"None", "Linear", "Exponential", "Step"}));
		configuration.add(
			"decayParam", dv::ConfigOption::doubleOption(
							  "Slope for linear decay, tau for exponential decay, time for step decay", 1e6, 0, 1e10));
		configuration.add("synchronousDecay", dv::ConfigOption::boolOption("Decay at frame generation time"));
		configuration.add(
			"accumulationTime", dv::ConfigOption::intOption("Time in ms to accumulate events over", 33, 1, 1000));
		configuration.add("accumulationNumber",
			dv::ConfigOption::intOption("Number of events to accumlate for a frame", 100000, 1, INT_MAX));
		configuration.add("colorDemosaicing", dv::ConfigOption::boolOption("Apply color demosaicing filter", false));

		configuration.setPriorityOptions(
			{"sliceMethod", "decayFunction", "decayParam", "synchronousDecay", "colorDemosaicing"});
	}

	void elaborateFrame(const dv::EventStore &events) {
		frameAccumulator.accumulate(events);
		// generate frame
		auto frame = frameAccumulator.generateFrame();

		// make sure frame is in correct exposure and data type
		double scaleFactor
			= 255.0 / static_cast<double>(frameAccumulator.getMaxPotential() - frameAccumulator.getMinPotential());
		double shiftFactor = -static_cast<double>(frameAccumulator.getMinPotential()) * scaleFactor;
		cv::Mat correctedFrame;
		frame.convertTo(correctedFrame, CV_8U, scaleFactor, shiftFactor);
		if (demosaic) {
			cv::Mat demosaicFrame;
			cv::cvtColor(correctedFrame, demosaicFrame, demosaic);
			outputs.getFrameOutput("frames") << currentFrameTime << demosaicFrame << dv::commit;
		}
		else {
			outputs.getFrameOutput("frames") << currentFrameTime << correctedFrame << dv::commit;
		}
	}

	void doPerFrameTime(const dv::EventStore &events) {
		if (currentFrameTime < 0) {
			currentFrameTime = events.getLowestTime();
		}

		currentFrameTime += accumulationTime;
		elaborateFrame(events);
	}

	void doPerEventNumber(const dv::EventStore &events) {
		currentFrameTime = events.getLowestTime();
		elaborateFrame(events);
	}

	Accumulator() : slicer(dv::EventStreamSlicer()) {
		sliceMethod = config.getString("sliceMethod");

		outputs.getFrameOutput("frames").setup(inputs.getEventInput("events"));
		frameAccumulator = dv::Accumulator(inputs.getEventInput("events").size(), dv::Accumulator::Decay::EXPONENTIAL,
			1e6, false, 0.04f, 0.3f, 0.0f, 0.0f, false);

		if (sliceMethod == "TIME") {
			accumulationTime = config.getInt("accumulationTime") * 1000;
			sliceJob         = slicer.doEveryTimeInterval(
                accumulationTime, std::function<void(const dv::EventStore &)>(
                                      std::bind(&Accumulator::doPerFrameTime, this, std::placeholders::_1)));
		}
		else if (sliceMethod == "NUMBER") {
			sliceJob = slicer.doEveryNumberOfEvents(config.getInt("accumulationNumber"),
				std::function<void(const dv::EventStore &)>(
					std::bind(&Accumulator::doPerEventNumber, this, std::placeholders::_1)));
		}
		else {
			// This condition should be impossible.
			throw std::runtime_error("Unknown slice method selected.");
		}
	}

	void run() override {
		slicer.accept(inputs.getEventInput("events").events());
	}

	static dv::Accumulator::Decay decayFromString(const std::string &name) {
		if (name == "Linear")
			return dv::Accumulator::Decay::LINEAR;
		if (name == "Exponential")
			return dv::Accumulator::Decay::EXPONENTIAL;
		if (name == "Step")
			return dv::Accumulator::Decay::STEP;
		return dv::Accumulator::Decay::NONE;
	}

	void configUpdate() override {
		frameAccumulator.setRectifyPolarity(config.getBool("rectifyPolarity"));
		frameAccumulator.setEventContribution(config.getFloat("eventContribution"));
		frameAccumulator.setMaxPotential(config.getFloat("maxPotential"));
		frameAccumulator.setNeutralPotential(config.getFloat("neutralPotential"));
		frameAccumulator.setMinPotential(config.getFloat("minPotential"));
		frameAccumulator.setDecayFunction(decayFromString(config.getString("decayFunction")));
		frameAccumulator.setDecayParam(config.getDouble("decayParam"));
		frameAccumulator.setSynchronousDecay(config.getBool("synchronousDecay"));

		if (sliceMethod != config.getString("sliceMethod")) {
			log.warning << "Please restart the module for 'sliceMethod' changes to take effect." << std::endl;
		}

		if (sliceMethod == "TIME") {
			accumulationTime = config.getInt("accumulationTime") * 1000;
			slicer.modifyTimeInterval(sliceJob, accumulationTime);
		}
		else {
			slicer.modifyNumberInterval(sliceJob, config.getInt("accumulationNumber"));
		}

		if (config.getBool("colorDemosaicing")) {
			if (inputs.getEventInput("events").infoNode().exists<dv::CfgType::INT>("colorFilter")) {
				demosaic = cv::COLOR_BayerBG2BGR + inputs.getEventInput("events").infoNode().getInt("colorFilter");
			}
			else {
				// Turn off again if not supported.
				config.setBool("colorDemosaicing", false);
				demosaic = 0;
			}
		}
		else {
			demosaic = 0;
		}
	}
};

registerModuleClass(Accumulator)
