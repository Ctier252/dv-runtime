# Compression support
FIND_LIBRARY(LIBLZ4 lz4)
IF (${LIBLZ4-NOTFOUND})
	MESSAGE(FATAL_ERROR "liblz4 not found, required for I/O compression.")
ENDIF()

FIND_LIBRARY(LIBZSTD zstd)
IF (${LIBZSTD-NOTFOUND})
	MESSAGE(FATAL_ERROR "libzstd not found, required for I/O compression.")
ENDIF()

# NET_TCP_SERVER
ADD_LIBRARY(output_net_tcp_server SHARED net_tcp_server.cpp)

SET_TARGET_PROPERTIES(output_net_tcp_server
	PROPERTIES
	PREFIX "dv_"
)

TARGET_LINK_LIBRARIES(output_net_tcp_server
	PRIVATE
		dvsdk
		Boost::boost
		${BOOST_ASIO_LIBRARIES}
		${LIBLZ4}
		${LIBZSTD})

INSTALL(TARGETS output_net_tcp_server DESTINATION ${DV_MODULES_DIR})

# NET_SOCKET
IF (NOT OS_WINDOWS)
	ADD_LIBRARY(output_net_socket SHARED net_socket.cpp)

	SET_TARGET_PROPERTIES(output_net_socket
		PROPERTIES
		PREFIX "dv_"
	)

	TARGET_LINK_LIBRARIES(output_net_socket
		PRIVATE
			dvsdk
			Boost::boost
			${BOOST_ASIO_LIBRARIES}
			${LIBLZ4}
			${LIBZSTD})

	INSTALL(TARGETS output_net_socket DESTINATION ${DV_MODULES_DIR})
ENDIF()

# FILE
ADD_LIBRARY(output_file SHARED file.cpp)

SET_TARGET_PROPERTIES(output_file
	PROPERTIES
	PREFIX "dv_"
)

TARGET_LINK_LIBRARIES(output_file
	PRIVATE
		dvsdk
		Boost::boost
		Boost::filesystem
		Boost::nowide
		${LIBLZ4}
		${LIBZSTD})

INSTALL(TARGETS output_file DESTINATION ${DV_MODULES_DIR})
