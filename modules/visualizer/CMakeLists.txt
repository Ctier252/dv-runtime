IF (OS_MACOSX)
	# This cannot work on MacOS X, due to graphics not being
	# run on the main thread anymore.
	RETURN()
ENDIF()

IF (NOT ENABLE_VISUALIZER)
	SET(ENABLE_VISUALIZER 0 CACHE BOOL "Enable the visualizer module. This is deprecated in favor of dv-gui!")
ENDIF()

IF (ENABLE_VISUALIZER)
	FIND_PACKAGE(SFML 2.3.0 COMPONENTS graphics)

	IF (NOT SFML_FOUND)
		# Search for external libraries with pkg-config.
		INCLUDE(FindPkgConfig)

		PKG_CHECK_MODULES(sfml-graphics REQUIRED IMPORTED_TARGET sfml-graphics>=2.3.0)

		SET(SFML_GRAPHICS_LIBS PkgConfig::sfml-graphics)
	ELSE()
		SET(SFML_GRAPHICS_LIBS sfml-graphics)
	ENDIF()

	ADD_LIBRARY(visualizer SHARED visualizer.cpp)

	SET_TARGET_PROPERTIES(visualizer
		PROPERTIES
		PREFIX "dv_"
	)

	TARGET_LINK_LIBRARIES(visualizer
		PRIVATE
			dvsdk
			${SFML_GRAPHICS_LIBS})

	INSTALL(TARGETS visualizer DESTINATION ${DV_MODULES_DIR})
ENDIF()
